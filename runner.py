import sys
import time

current = time.localtime()

with open(sys.argv[1], 'a') as f:
    f.write("Hello world it is {}/{} {}:{}:{}\n".format(current.tm_mon,current.tm_mday,current.tm_hour,current.tm_min,current.tm_sec))
